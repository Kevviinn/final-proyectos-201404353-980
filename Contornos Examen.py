
import cv2

# Cargamos la imagen
original = cv2.imread("1.jpg")
cv2.imshow("original", original)

# Convertimos a escala de grises
gris = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)

# Aplicar suavizado Gaussiano
gauss = cv2.GaussianBlur(gris, (5, 5), 0)

# Detectamos los bordes con Canny
canny = cv2.Canny(gauss, 100, 100) #si se la baja aumenta la sensibilidad
cv2.imwrite("fo.png",canny)
cv2.imshow("canny", canny)
# Buscamos los contornos
(contornos, _) = cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

print("He encontrado {} objetos".format(len(contornos)))

cv2.drawContours(original, contornos, -1, (0, 0, 255), 8)
cv2.imwrite("fot.png",original)
cv2.imshow("contornos", original)


cv2.waitKey(0)